//=====================================================================================================
// MadgwickAHRS.h
//=====================================================================================================
//
// Implementation of Madgwick's IMU and AHRS algorithms.
// See: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
//
// Date			Author          Notes
// 29/09/2011	SOH Madgwick    Initial release
// 02/10/2011	SOH Madgwick	Optimised for reduced CPU load
// 19/02/2012	SOH Madgwick	Magnetometer measurement is normalised
// 11/03/2015	Mads Bornebusch	Changed sample frequency to sample period
// 19/03/2015	Mads Bornebusch Added function to set beta
// 12/04/2015	Mads Bornebusch	Changed the quaternion to a struct
// 25/05/2015	Mads Bornebusch Added function to get beta
// 27/05/2015	Mads Bornebusch Deleted some external variables. Made DT (sample period) a define instead
//
//=====================================================================================================
#ifndef MadgwickAHRS_h
#define MadgwickAHRS_h


struct quaternion{
  float q0, q1, q2, q3;
};

//---------------------------------------------------------------------------------------------------
// Function declarations

quaternion getQuaternion();
void setBeta(float betaIn); 
float getBeta(); 
void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az);

#endif
//=====================================================================================================
// End of file
//=====================================================================================================
