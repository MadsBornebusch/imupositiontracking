/* Written by Mads Bornebusch 2015.
 This software may be distributed and modified under the terms of the GNU
 General Public License version 2 (GPL2) as published by the Free Software
 Foundation and appearing in the file GPL2.TXT included in the packaging of
 this file. Please note that GPL2 Section 2[b] requires that all works based
 on this software must also be made publicly available under the terms of
 the GPL2 ("Copyleft").
  
 This file is based on:
 I2C file for MPU6050 by Kristian Lauszus
 https://github.com/TKJElectronics/Example-Sketch-for-IMU-including-Kalman-filter/blob/master/IMU/MPU6050/I2C.ino
 
 */


#include <Arduino.h>
#include <Wire.h> 

#include "I2C.h"

const uint16_t I2C_TIMEOUT = 1000;

void initI2C(){
  Wire.begin();
  // TWBR = ((F_CPU / 400000L) - 16) / 2; // I2C frequency is set to 400kHz
}


uint8_t i2cWrite(uint8_t I2Caddress, uint8_t registerAddress, uint8_t data){
  Wire.beginTransmission(I2Caddress);
  Wire.write((byte)registerAddress);
  Wire.write(data);
  uint8_t rcode = Wire.endTransmission(true); // Returns 0 on success
  if (rcode) {
    Serial.print(F("i2cWrite failed: "));
    Serial.println(rcode);
  }
  return rcode; // See: http://arduino.cc/en/Reference/WireEndTransmission
}


uint8_t i2cRead(uint8_t I2Caddress, uint8_t registerAddress, uint8_t *data, uint8_t nbytes) {
  uint32_t timeOutTimer;
  Wire.beginTransmission(I2Caddress);
  Wire.write(registerAddress);
  uint8_t rcode = Wire.endTransmission();
  if (rcode) {
    Serial.print(F("i2cRead failed: "));
    Serial.println(rcode);
    return rcode; // See: http://arduino.cc/en/Reference/WireEndTransmission
  } 
  Wire.requestFrom(I2Caddress, (byte)nbytes); // Send a repeated start and then release the bus after reading
  timeOutTimer = micros();
  while((Wire.available() < nbytes) && ((micros() - timeOutTimer) < I2C_TIMEOUT) );
  if (Wire.available()>=nbytes){
    for (uint8_t i = 0; i < nbytes; i++)
      data[i] = Wire.read();
  }else {
    if(((micros() - timeOutTimer) >= I2C_TIMEOUT)){
      Serial.println(F("i2cRead timeout"));
      return 5; // This error value is not already taken by endTransmission
    }
  }
  return 0; // Success
}
