/* Written by Mads Bornebusch 2015.
 This software may be distributed and modified under the terms of the GNU
 General Public License version 2 (GPL2) as published by the Free Software
 Foundation and appearing in the file GPL2.TXT included in the packaging of
 this file. Please note that GPL2 Section 2[b] requires that all works based
 on this software must also be made publicly available under the terms of
 the GPL2 ("Copyleft").

 This file is based on a library for the L3GD20 and L3GD20H GYROSCOPE
 and the library for the LSM303 Accelerometer/magnentometer
 all of which was written by Kevin "KTOWN" Townsend for Adafruit Industries.
 Gyroscope library: https://github.com/adafruit/Adafruit_L3GD20
 Acc/Mag library: https://github.com/adafruit/Adafruit_LSM303

 This file is designed to work with the Adafruit 10-DOF IMU breakout 
 http://www.adafruit.com/products/1604.

 All data in this file is based on the following datasheets:
 Gyroscope: http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00060659.pdf
 Acc/Mag: http://www.adafruit.com/datasheets/LSM303DLHC.PDF
 */


#include <Arduino.h>
#include <Wire.h>
#include "Adafruit10dof.h"
#include "I2C.h"
#include "IMUPositionTracking.h"



// Gyro address
#define L3GD20_ADDRESS 0x6B
 // Gyro IDs
#define L3GD20_ID  0xD4
#define L3GD20H_ID 0xD7

// Accelerometer address
#define LSM303_ADDRESS_ACCEL  (0x32 >> 1)         // 0011001x
 // Magnetometer address
#define LSM303_ADDRESS_MAG    (0x3C >> 1)         // 0011110x
 // Acc/Mag chip ID
#define LSM303_ID 0xD4


// Accelerometer range
#define ACC_RANGE 8 // Set to 2, 4, 8 or 16 corresponding to ±2g, ±4g, ±8g or ±16g
// Defining accelerometer scale register


#if ACC_RANGE == 2
  #define LSM303_SCALE (0<<4) 
#elif ACC_RANGE == 4
  #define LSM303_SCALE (1<<4)
#elif ACC_RANGE == 8
  #define LSM303_SCALE (2<<4)
#elif ACC_RANGE == 16
  #define LSM303_SCALE (3<<4)
#endif

// Gyroscope range
#define GYRO_RANGE 500 // Set to 245, 500 or 2000 corresponding to ±245deg/s, ±500deg/s or ±2000deg/s
// Definig gyrocospe scale register
#if GYRO_RANGE == 245
  #define L3GD20_SCALE (0<<4)
#elif GYRO_RANGE == 500
  #define L3GD20_SCALE (1<<4)
#elif GYRO_RANGE == 2000
  #define L3GD20_SCALE (3<<4)
#endif

//Gyro register definitions           // DEFAULT    TYPE
#define L3GD20_REGISTER_WHO_AM_I        0x0F  // 11010100   r
#define L3GD20_REGISTER_CTRL_REG1       0x20  // 00000111   rw
#define L3GD20_REGISTER_CTRL_REG2       0x21  // 00000000   rw
#define L3GD20_REGISTER_CTRL_REG3       0x22  // 00000000   rw
#define L3GD20_REGISTER_CTRL_REG4       0x23  // 00000000   rw
#define L3GD20_REGISTER_CTRL_REG5       0x24  // 00000000   rw
#define L3GD20_REGISTER_REFERENCE       0x25  // 00000000   rw
#define L3GD20_REGISTER_OUT_TEMP        0x26  //            r
#define L3GD20_REGISTER_STATUS_REG      0x27  //            r
#define L3GD20_REGISTER_OUT_X_L         0x28  //            r
#define L3GD20_REGISTER_OUT_X_H         0x29  //            r
#define L3GD20_REGISTER_OUT_Y_L         0x2A  //            r
#define L3GD20_REGISTER_OUT_Y_H         0x2B  //            r
#define L3GD20_REGISTER_OUT_Z_L         0x2C  //            r
#define L3GD20_REGISTER_OUT_Z_H         0x2D  //            r
#define L3GD20_REGISTER_FIFO_CTRL_REG   0x2E  // 00000000   rw
#define L3GD20_REGISTER_FIFO_SRC_REG    0x2F  //            r
#define L3GD20_REGISTER_INT1_CFG        0x30  // 00000000   rw
#define L3GD20_REGISTER_INT1_SRC        0x31  //            r
#define L3GD20_REGISTER_TSH_XH          0x32  // 00000000   rw
#define L3GD20_REGISTER_TSH_XL          0x33  // 00000000   rw
#define L3GD20_REGISTER_TSH_YH          0x34  // 00000000   rw
#define L3GD20_REGISTER_TSH_YL          0x35  // 00000000   rw
#define L3GD20_REGISTER_TSH_ZH          0x36  // 00000000   rw
#define L3GD20_REGISTER_TSH_ZL          0x37  // 00000000   rw
#define L3GD20_REGISTER_INT1_DURATION   0x38  // 00000000   rw




// Accelerometer register definitions         //DEFAULT    TYPE
#define LSM303_REGISTER_ACCEL_CTRL_REG1_A       0x20  // 00000111   rw
#define LSM303_REGISTER_ACCEL_CTRL_REG2_A       0x21  // 00000000   rw
#define LSM303_REGISTER_ACCEL_CTRL_REG3_A       0x22  // 00000000   rw
#define LSM303_REGISTER_ACCEL_CTRL_REG4_A       0x23  // 00000000   rw
#define LSM303_REGISTER_ACCEL_CTRL_REG5_A       0x24  // 00000000   rw
#define LSM303_REGISTER_ACCEL_CTRL_REG6_A       0x25  // 00000000   rw
#define LSM303_REGISTER_ACCEL_REFERENCE_A       0x26  // 00000000   r
#define LSM303_REGISTER_ACCEL_STATUS_REG_A      0x27  // 00000000   r
#define LSM303_REGISTER_ACCEL_OUT_X_L_A         0x28
#define LSM303_REGISTER_ACCEL_OUT_X_H_A         0x29
#define LSM303_REGISTER_ACCEL_OUT_Y_L_A         0x2A
#define LSM303_REGISTER_ACCEL_OUT_Y_H_A         0x2B
#define LSM303_REGISTER_ACCEL_OUT_Z_L_A         0x2C
#define LSM303_REGISTER_ACCEL_OUT_Z_H_A         0x2D
#define LSM303_REGISTER_ACCEL_FIFO_CTRL_REG_A 0x2E
#define LSM303_REGISTER_ACCEL_FIFO_SRC_REG_A    0x2F
#define LSM303_REGISTER_ACCEL_INT1_CFG_A        0x30
#define LSM303_REGISTER_ACCEL_INT1_SOURCE_A     0x31
#define LSM303_REGISTER_ACCEL_INT1_THS_A        0x32
#define LSM303_REGISTER_ACCEL_INT1_DURATION_A   0x33
#define LSM303_REGISTER_ACCEL_INT2_CFG_A        0x34
#define LSM303_REGISTER_ACCEL_INT2_SOURCE_A     0x35
#define LSM303_REGISTER_ACCEL_INT2_THS_A        0x36
#define LSM303_REGISTER_ACCEL_INT2_DURATION_A   0x37
#define LSM303_REGISTER_ACCEL_CLICK_CFG_A       0x38
#define LSM303_REGISTER_ACCEL_CLICK_SRC_A       0x39
#define LSM303_REGISTER_ACCEL_CLICK_THS_A       0x3A
#define LSM303_REGISTER_ACCEL_TIME_LIMIT_A      0x3B
#define LSM303_REGISTER_ACCEL_TIME_LATENCY_A    0x3C
#define LSM303_REGISTER_ACCEL_TIME_WINDOW_A     0x3D


// Magnetometer register definitions
#define LSM303_REGISTER_MAG_CRA_REG_M       0x00
#define LSM303_REGISTER_MAG_CRB_REG_M       0x01
#define LSM303_REGISTER_MAG_MR_REG_M        0x02
#define LSM303_REGISTER_MAG_OUT_X_H_M       0x03
#define LSM303_REGISTER_MAG_OUT_X_L_M       0x04
#define LSM303_REGISTER_MAG_OUT_Z_H_M       0x05
#define LSM303_REGISTER_MAG_OUT_Z_L_M       0x06
#define LSM303_REGISTER_MAG_OUT_Y_H_M       0x07
#define LSM303_REGISTER_MAG_OUT_Y_L_M       0x08
#define LSM303_REGISTER_MAG_SR_REG_Mg       0x09
#define LSM303_REGISTER_MAG_IRA_REG_M       0x0A
#define LSM303_REGISTER_MAG_IRB_REG_M       0x0B
#define LSM303_REGISTER_MAG_IRC_REG_M       0x0C
#define LSM303_REGISTER_MAG_TEMP_OUT_H_M    0x31
#define LSM303_REGISTER_MAG_TEMP_OUT_L_M    0x32


// Magnetometer  scale
#define   LSM303_MAGGAIN_1_3  0x20  // +/- 1.3
#define   LSM303_MAGGAIN_1_9  0x40  // +/- 1.9
#define   LSM303_MAGGAIN_2_5  0x60  // +/- 2.5
#define   LSM303_MAGGAIN_4_0  0x80  // +/- 4.0
#define   LSM303_MAGGAIN_4_7  0xA0  // +/- 4.7
#define   LSM303_MAGGAIN_5_6  0xC0  // +/- 5.6
#define   LSM303_MAGGAIN_8_1  0xE0  // +/- 8.1 

// Conversion factors
#define RAW2DEG ((float)GYRO_RANGE/((float)32768)) // =GYRO_RANGE/((2^16)/2)
#define GYRORAW2RAD RAW2DEG*DEG2RAD //Raw to rad/s conversion factor for the gyro

#define ACCRAW2G ((float)ACC_RANGE/((float)32768)) // = ACC_RANGE/((2^16)/2)
#define ACCG2MSS 9.82f // Gravitational acceleration (Unit: m/s/s)
#define ACCRAW2MSS ACCRAW2G*ACCG2MSS // Raw to m/s/s acceleration conversion

// Terminal output for debugging
#define CAL_OUTPUT 1 // Set to 1 to get output from the calibration
#define GYRO_ERR_TEST 0 // Prints the maximum gyro error after calibration

#define ACC_AUTO_CAL 1

// Gyro calibration variables
double gyroX_cal, gyroY_cal, gyroZ_cal;

// TODO: Move these to EEPROM and load them from there
// Accelerometer calibration values
double accX_cal = -109.18; 
double accY_cal = -8.38;
double accZ_cal = -43.39; 
// Magnetometer calibration values
double magX_cal = 93.9;
double magY_cal = -67.4;
double magZ_cal = -44.6;


uint32_t timer;
// TODO: Change to approptiate size
uint8_t i2cData[21]; // Buffer for I2C data

// TODO: Read barometric pressure and temperature

void Adafruit10DOF_SetupGyro(){
  // Read Gyro Who am I register
  while (i2cRead(L3GD20_ADDRESS, L3GD20_REGISTER_WHO_AM_I, i2cData, 1));
  // Serial.println(i2cData[0], HEX);
  if ((i2cData[0] != L3GD20_ID) && (i2cData[0] != L3GD20H_ID)) { // Read "WHO_AM_I" register
    Serial.print(F("Error reading sensor"));
    while (1);
  } 

  /* Set CTRL_REG1 (0x20)
   ====================================================================
   BIT  Symbol    Description                                   Default
   ---  ------    --------------------------------------------- -------
   7-6  DR1/0     Output data rate                                   00
   5-4  BW1/0     Bandwidth selection                                00
     3  PD        0 = Power-down mode, 1 = normal/sleep mode          0
     2  ZEN       Z-axis enable (0 = disabled, 1 = enabled)           1
     1  YEN       Y-axis enable (0 = disabled, 1 = enabled)           1
     0  XEN       X-axis enable (0 = disabled, 1 = enabled)           1 */

  //Switch to normal mode and enable all three channels
  while (i2cWrite(L3GD20_ADDRESS, L3GD20_REGISTER_CTRL_REG1, 0x0F));

 
  /* Set CTRL_REG2 (0x21)
   ====================================================================
   BIT  Symbol    Description                                   Default
   ---  ------    --------------------------------------------- -------
   5-4  HPM1/0    High-pass filter mode selection                    00
   3-0  HPCF3..0  High-pass filter cutoff frequency selection      0000 */

  // Nothing to do ... keep default values
  

  /* Set CTRL_REG3 (0x22)
   ====================================================================
   BIT  Symbol    Description                                   Default
   ---  ------    --------------------------------------------- -------
     7  I1_Int1   Interrupt enable on INT1 (0=disable,1=enable)       0
     6  I1_Boot   Boot status on INT1 (0=disable,1=enable)            0
     5  H-Lactive Interrupt active config on INT1 (0=high,1=low)      0
     4  PP_OD     Push-Pull/Open-Drain (0=PP, 1=OD)                   0
     3  I2_DRDY   Data ready on DRDY/INT2 (0=disable,1=enable)        0
     2  I2_WTM    FIFO wtrmrk int on DRDY/INT2 (0=dsbl,1=enbl)        0
     1  I2_ORun   FIFO overrun int on DRDY/INT2 (0=dsbl,1=enbl)       0
     0  I2_Empty  FIFI empty int on DRDY/INT2 (0=dsbl,1=enbl)         0 */

  // Nothing to do ... keep default values


  /* Set CTRL_REG4 (0x23)
   ====================================================================
   BIT  Symbol    Description                                   Default
   ---  ------    --------------------------------------------- -------
     7  BDU       Block Data Update (0=continuous, 1=LSB/MSB)         0
     6  BLE       Big/Little-Endian (0=Data LSB, 1=Data MSB)          0
   5-4  FS1/0     Full scale selection                               00
                                  00 = 245 dps
                                  01 = 500 dps
                                  10 = 2000 dps
                                  11 = 2000 dps
     0  SIM       SPI Mode (0=4-wire, 1=3-wire)                       0 */

// Set to 500 deg/s
 while (i2cWrite(L3GD20_ADDRESS, L3GD20_REGISTER_CTRL_REG4, L3GD20_SCALE ));


 /* Set CTRL_REG5 (0x24)
   ====================================================================
   BIT  Symbol    Description                                   Default
   ---  ------    --------------------------------------------- -------
     7  BOOT      Reboot memory content (0=normal, 1=reboot)          0
     6  FIFO_EN   FIFO enable (0=FIFO disable, 1=enable)              0
     4  HPen      High-pass filter enable (0=disable,1=enable)        0
   3-2  INT1_SEL  INT1 Selection config                              00
   1-0  OUT_SEL   Out selection config                               00 */

  // Nothing to do ... keep default values

}
  

void Adafruit10DOF_SetupAcc(){

   /* Accelerometer CTRL_REG1_A
  ====================================================================
   BIT  Symbol    Description                                   Default
   ---  ------    --------------------------------------------- -------
    7-4  ODR3-0   Data rate selection.                           0
      3  LPen     Low-power mode enable.                         0
      2  Zen      Z axis enable.                                 1
      1  Yen      Y axis enable.                                 1
      0  Xen      X axis enable.                                 1

  ====================================================================
  ODR3 ODR2 ODR1 ODR0 Power mode selection
    0   0   0   0     Power-down mode
    0   0   0   1     Normal / low-power mode (1 Hz)
    0   0   1   0     Normal / low-power mode (10 Hz)
    0   0   1   1     Normal / low-power mode (25 Hz)
    0   1   0   0     Normal / low-power mode (50 Hz)
    0   1   0   1     Normal / low-power mode (100 Hz)
    0   1   1   0     Normal / low-power mode (200 Hz)
    0   1   1   1     Normal / low-power mode (400 Hz)
    1   0   0   0     Low-power mode (1.620 KHz)
    1   0   0   1     Normal (1.344 kHz) / low-power mode (5.376 KHz)
    */

  while (i2cWrite(LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG1_A, 0x77));

  /*
  Accelerometer CTRL_REG4_A
  ============================
  BDU BLE FS1 FS0 HR 0 0 SIM
  ============================
  BDU Block data update. Default value: 0
        (0: continuos update, 1: output registers not updated until MSB and LSB reading
  BLE Big/little endian data selection. Default value 0.
        (0: data LSB @ lower address, 1: data MSB @ lower address)
  FS1-FS0 Full-scale selection. Default value: 00
        (00: +/- 2G, 01: +/- 4G, 10: +/- 8G, 11: +/- 16G)
  HR High resolution output mode: Default value: 0
        (0: high resolution disable, 1: high resolution enable)
  SIM SPI serial interface mode selection. Default value: 0
        (0: 4-wire interface, 1: 3-wire interface).
  */

  while (i2cWrite(LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG4_A, LSM303_SCALE ));      

}


void Adafruit10DOF_SetupMag(){

  /*
  Magnetometer CRA_REG_M register 
  ============================
  TEMP_EN 0 0 DO2 DO1 DO0 0 0
  ============================
  (Zeros must be set zero for correct working of device)

  TEMP _EN  Temperature sensor enable.
            0: temperature sensor disabled (default), 1: temperature sensor enabled
  DO2 to DO0  Data output rate bits. These bits set the rate at which data is written to all three data
              output registers (refer to Table 72). Default value: 100

  DO2 DO1 DO0   Minimum data output rate (Hz)
    0   0   0   0.75
    0   0   1   1.5
    0   1   0   3.0
    0   1   1   7.5
    1   0   0   15
    1   0   1   30
    1   1   0   75
    1   1   1   220
  */
  // Set to minimum 220Hz output rate
  while (i2cWrite(LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRA_REG_M, (7<<2) ));


  /*
  Magnetometer CRB_REG_M register
  =======================
  GN2 GN1 GN0 0 0 0 0 0
  =======================
  (Zeros must be set zero for correct working of device)

  GN2 GN1 GN0   Sensor input field range [Gauss]    Gain X, Y, and Z [LSB/Gauss]    Gain Z [LSB/Gauss] 
  0   0   1     ±1.3                                1100                            980
  0   1   0     ±1.9                                855                             760
  0   1   1     ±2.5                                670                             600
  1   0   0     ±4.0                                450                             400
  1   0   1     ±4.7                                400                             355
  1   1   0     ±5.6                                330                             295
  1   1   1     ±8.1                                230                             205

  Output range
  0xF800–0x07FF
  (-2048–2047)
  */
  while (i2cWrite(LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRB_REG_M, LSM303_MAGGAIN_1_3)); 


  /*
  Magnetometer MR_REG_M register
  =====================
  0 0 0 0 0 0 MD1 MD0
  =====================

  MD1-0 Mode select bits. These bits select the operation mode of this device 

  MD1 MD0 Mode
  0   0   Continuous-conversion mode
  0   1   Single-conversion mode
  1   0   Sleep-mode. Device is placed in sleep-mode
  1   1   Sleep-mode. Device is placed in sleep-mode
  */

  // Set to continuous conversion mode
  while (i2cWrite(LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_MR_REG_M, 0x00));
}


struct IMUdatatype Adafruit10DOF_readData(){
  /* Update all the values */
  // Calibration values (mean of 10000 measurements with the IMU being stationary) are subtracted below

  timer = micros();
  struct IMUdatatype IMU;

  //Read Gyro data
  while (i2cRead(L3GD20_ADDRESS, (L3GD20_REGISTER_OUT_X_L | 0x80), i2cData, 6));
  IMU.gyroX = ((float)((int16_t)(i2cData[0] | (i2cData[1] << 8))) - gyroX_cal) * GYRORAW2RAD;
  IMU.gyroY = ((float)((int16_t)(i2cData[2] | (i2cData[3] << 8))) - gyroY_cal) * GYRORAW2RAD;
  IMU.gyroZ = ((float)((int16_t)(i2cData[4] | (i2cData[5] << 8))) - gyroZ_cal) * GYRORAW2RAD;


  // Read Accelerometer data
  while (i2cRead(LSM303_ADDRESS_ACCEL, (LSM303_REGISTER_ACCEL_OUT_X_L_A | 0x80), i2cData, 6));
  IMU.accX = ((float)((int16_t)(i2cData[0] | (i2cData[1] << 8))) - accX_cal) * ACCRAW2MSS; 
  IMU.accY = ((float)((int16_t)(i2cData[2] | (i2cData[3] << 8))) - accY_cal) * ACCRAW2MSS;
  IMU.accZ = ((float)((int16_t)(i2cData[4] | (i2cData[5] << 8))) - accZ_cal) * ACCRAW2MSS;


  // Read Magnetometer data
  while (i2cRead(LSM303_ADDRESS_MAG, (LSM303_REGISTER_MAG_OUT_X_H_M | 0x80), i2cData, 6));
  IMU.magX = ((int16_t)(i2cData[1] | (i2cData[0] << 8))) - magX_cal; 
  IMU.magZ = ((int16_t)(i2cData[3] | (i2cData[2] << 8))) - magY_cal;
  IMU.magY = ((int16_t)(i2cData[5] | (i2cData[4] << 8))) - magZ_cal;

  IMU.temp = 0.0;
  IMU.pressure = 0.0;

  IMU.dt = micros() - timer ; //Calculate the time it took to acquire the data in microseconds
  
  return IMU;
}


struct IMUdatatype Adafruit10DOF_readRawData(){
  /* Update all the values */

  timer = micros();
  struct IMUdatatype IMU;


  // Read Gyro data
  while (i2cRead(L3GD20_ADDRESS, (L3GD20_REGISTER_OUT_X_L | 0x80), i2cData, 6));
  IMU.gyroX = ((int16_t)(i2cData[0] | (i2cData[1] << 8)));
  IMU.gyroY = ((int16_t)(i2cData[2] | (i2cData[3] << 8)));
  IMU.gyroZ = ((int16_t)(i2cData[4] | (i2cData[5] << 8)));

  // Read accelerometer data
  while (i2cRead(LSM303_ADDRESS_ACCEL, (LSM303_REGISTER_ACCEL_OUT_X_L_A | 0x80), i2cData, 6));
  IMU.accX = ((int16_t)(i2cData[0] | (i2cData[1] << 8))) ; 
  IMU.accY = ((int16_t)(i2cData[2] | (i2cData[3] << 8))) ;
  IMU.accZ = ((int16_t)(i2cData[4] | (i2cData[5] << 8))) ;

  // Read Magnetometer data
  while (i2cRead(LSM303_ADDRESS_MAG, (LSM303_REGISTER_MAG_OUT_X_H_M | 0x80), i2cData, 6));
  IMU.magX = ((int16_t)(i2cData[1] | (i2cData[0] << 8))); 
  IMU.magZ = ((int16_t)(i2cData[3] | (i2cData[2] << 8)));
  IMU.magY = ((int16_t)(i2cData[5] | (i2cData[4] << 8)));

  IMU.temp = 0.0;
  IMU.pressure = 0.0;

  IMU.dt = micros() - timer ; //Calculate the time it took to acquire the data in microseconds
  
  return IMU;
}

float measAccCalib(int axis){
  Serial.print("Measuring acceleration\r\n"); 
  float IMUmean = 0.0;
  IMUdatatype IMU; 
  int now = millis();
  int looptimer; 

  while ((int)(millis() - now) < 1000) { // Calibrate for n seconds
    looptimer = micros(); 
    IMU = Adafruit10DOF_readRawData();  // Get raw data from the IMU

    switch(axis){
      case 0:
        IMUmean += IMU.accX;
        break;
      case 1:
        IMUmean += IMU.accY;
        break; 
      case 2:
        IMUmean += IMU.accZ;
        break; 
    }
    while ((micros()-looptimer) < LOOPTIME); // Keeping the loop running a fixed frequency
  }
  return IMUmean/(1.0/DT);
}

void calibrateAcc() {
  
  Serial.print("Accelerometer calibrating. Release button!\r\n"); 
  while(!digitalRead(BUTTON_PIN)); 

  double accZeroMinX, accZeroMinY, accZeroMinZ; 
  double accZeroMaxX, accZeroMaxY, accZeroMaxZ; 

  // TODO: Take a mean of several values

  delay(1000);
  Serial.print("Place accelerometer with X-axis up and click button\r\n"); 
  while(digitalRead(BUTTON_PIN));
  accZeroMaxX = measAccCalib(0);
  while(!digitalRead(BUTTON_PIN));

  Serial.print("Place accelerometer with X-axis down and click button\r\n"); 
  while(digitalRead(BUTTON_PIN));
  accZeroMinX = measAccCalib(0);
  while(!digitalRead(BUTTON_PIN));

  Serial.print("Place accelerometer with Y-axis up and click button\r\n"); 
  while(digitalRead(BUTTON_PIN));
  accZeroMaxY = measAccCalib(1);
  while(!digitalRead(BUTTON_PIN));

  Serial.print("Place accelerometer with Y-axis down and click button\r\n"); 
  while(digitalRead(BUTTON_PIN));
  accZeroMinY = measAccCalib(1);
  while(!digitalRead(BUTTON_PIN));

  Serial.print("Place accelerometer with Z-axis up and click button\r\n"); 
  while(digitalRead(BUTTON_PIN));
  accZeroMaxZ = measAccCalib(2);
  while(!digitalRead(BUTTON_PIN));

  Serial.print("Place accelerometer with Z-axis down and click button\r\n"); 
  while(digitalRead(BUTTON_PIN));
  accZeroMinZ = measAccCalib(2);
  while(!digitalRead(BUTTON_PIN));

  
  // Finding how much the current calibration values are changed
  float accDiffX = ((accZeroMaxX + accZeroMinX) / 2.0f) - accX_cal; 
  float accDiffY = ((accZeroMaxY + accZeroMinY) / 2.0f) - accY_cal;
  float accDiffZ = ((accZeroMaxZ + accZeroMinZ) / 2.0f) - accZ_cal;

  // Calculating the calibration values
  accX_cal = (accZeroMaxX + accZeroMinX) / 2.0f; 
  accY_cal = (accZeroMaxY + accZeroMinY) / 2.0f;
  accZ_cal = (accZeroMaxZ + accZeroMinZ) / 2.0f;
    
  // Printing the calibration values
  Serial.print("Accelerometer measurements: "); 
  Serial.print(accZeroMinX); Serial.print("\t");
  Serial.print(accZeroMaxX); Serial.print("\t");
  Serial.print(accZeroMinY); Serial.print("\t");
  Serial.print(accZeroMaxY); Serial.print("\t");
  Serial.print(accZeroMinZ); Serial.print("\t");
  Serial.print(accZeroMaxZ); Serial.print("\r\n");
  Serial.print("Widths: ");
  Serial.print(accZeroMaxX - accZeroMinX); Serial.print("\t");
  Serial.print(accZeroMaxY - accZeroMinY); Serial.print("\t");
  Serial.print(accZeroMaxZ - accZeroMinZ); Serial.print("\r\n"); 
  Serial.print("Calibration values: "); 
  Serial.print(accX_cal); Serial.print("\t");
  Serial.print(accY_cal); Serial.print("\t");
  Serial.print(accZ_cal); Serial.print("\r\n");
  Serial.print("Difference from current calibration values: "); 
  Serial.print(accDiffX); Serial.print("\t");
  Serial.print(accDiffY); Serial.print("\t");
  Serial.print(accDiffZ); Serial.print("\r\n");
  Serial.print("1g level: "); Serial.print(32768/ACC_RANGE); Serial.print("\r\n");

}

void calibrateMag(int n) {
  // n is the number of seconds the magnetometer calibration is running
  if(n<1)
    n=1; 
  
  IMUdatatype IMU;
  double magZeroMinX, magZeroMinY, magZeroMinZ; 
  double magZeroMaxX, magZeroMaxY, magZeroMaxZ; 

  Serial.print("Magnetometer calibrating.\r\nYou have "); Serial.print(n); 
  Serial.print(" seconds to turn the magnetometer around all axes.\r\n");
  
  IMU = Adafruit10DOF_readRawData(); 

  magZeroMinX = IMU.magX;
  magZeroMaxX = IMU.magX;
  magZeroMinY = IMU.magY;
  magZeroMaxY = IMU.magY;
  magZeroMinZ = IMU.magZ;
  magZeroMaxZ = IMU.magZ;
  

  int now = millis();
  while ((int)(millis() - now) < (n*1000)) { // Calibrate for n seconds
    int looptimer = micros(); 

    IMU = Adafruit10DOF_readRawData();  // Get raw data from the IMU

    // Check if we have a new min/max and update the value if that is the case
    if(magZeroMinX > IMU.magX)
      magZeroMinX = IMU.magX;
    if(magZeroMaxX < IMU.magX)
      magZeroMaxX = IMU.magX;
    if(magZeroMinY > IMU.magY)
      magZeroMinY = IMU.magY;
    if(magZeroMaxY < IMU.magY)
      magZeroMaxY = IMU.magY;
    if(magZeroMinZ > IMU.magZ)
      magZeroMinZ = IMU.magZ;
    if(magZeroMaxZ < IMU.magZ)
      magZeroMaxZ = IMU.magZ;

    while ((micros()-looptimer) < LOOPTIME); // Keeping the loop running a fixed frequency
  }

  // Finding how much the current calibration values are changed
  float magDiffX = ((magZeroMaxX + magZeroMinX) / 2.0f) - magX_cal; 
  float magDiffY = ((magZeroMaxY + magZeroMinY) / 2.0f) - magY_cal;
  float magDiffZ = ((magZeroMaxZ + magZeroMinZ) / 2.0f) - magZ_cal;

  // Calculating the calibration values
  magX_cal = (magZeroMaxX + magZeroMinX) / 2.0f; 
  magY_cal = (magZeroMaxY + magZeroMinY) / 2.0f;
  magZ_cal = (magZeroMaxZ + magZeroMinZ) / 2.0f;
    
  // Printing the calibration values
  Serial.print("Measurements: "); 
  Serial.print(magZeroMinX); Serial.print("\t");
  Serial.print(magZeroMaxX); Serial.print("\t");
  Serial.print(magZeroMinY); Serial.print("\t");
  Serial.print(magZeroMaxY); Serial.print("\t");
  Serial.print(magZeroMinZ); Serial.print("\t");
  Serial.print(magZeroMaxZ); Serial.print("\r\n");
  Serial.print("Widths: ");
  Serial.print(magZeroMaxX - magZeroMinX); Serial.print("\t");
  Serial.print(magZeroMaxY - magZeroMinY); Serial.print("\t");
  Serial.print(magZeroMaxZ - magZeroMinZ); Serial.print("\r\n"); 
  Serial.print("Calibration values: "); 
  Serial.print(magX_cal); Serial.print("\t");
  Serial.print(magY_cal); Serial.print("\t");
  Serial.print(magZ_cal); Serial.print("\r\n");
  Serial.print("Difference from current calibration values: "); 
  Serial.print(magDiffX); Serial.print("\t");
  Serial.print(magDiffY); Serial.print("\t");
  Serial.print(magDiffZ); Serial.print("\r\n");

}


void calibrateGyro(int n){
  // n is the number of samples used to find the calibration values
  int i;
  IMUdatatype IMU;
  
  #if CAL_OUTPUT
    Serial.print("Starting gyro Calibration");
    Serial.print("\r\n");
  #endif

  for(i=0;i<=n;i++){
    int looptimer = micros(); 
    IMU = Adafruit10DOF_readRawData();
    #if ACC_AUTO_CAL
      accX_cal += (double)IMU.accX/(double)n;
      accY_cal += (double)IMU.accY/(double)n; 
      accZ_cal += (double)IMU.accZ/(double)n; 
    #endif
    gyroX_cal += (double)IMU.gyroX/(double)n; 
    gyroY_cal += (double)IMU.gyroY/(double)n; 
    gyroZ_cal += (double)IMU.gyroZ/(double)n;
    while ((micros()-looptimer) <LOOPTIME); // Keeping the loop running a fixed frequency
  }
  #if ACC_AUTO_CAL
    accZ_cal -= (32768/ACC_RANGE); // ((2^16)/2)/ACC_RANGE. This number corresponds to 1g
  #endif

  #if CAL_OUTPUT
    Serial.print("\r\nPrinting calibration values\r\n");
    #if ACC_AUTO_CAL
      Serial.print("Acc: \t");
      Serial.print(accX_cal); Serial.print("\t");
      Serial.print(accY_cal); Serial.print("\t");
      Serial.print(accZ_cal); Serial.print("\r\n");
    #endif
    Serial.print("Gyro: \t");
    Serial.print(gyroX_cal); Serial.print("\t");
    Serial.print(gyroY_cal); Serial.print("\t");
    Serial.print(gyroZ_cal); Serial.print("\r\n");
    
    Serial.print("Done! \r\n");
  #endif


  #if GYRO_ERR_TEST
    Serial.print("Finding gyroscope error after calibration. This takes 5 seconds\r\n");
    float gyroMaxErr = 0.0;
    int now = millis(); 
    while((millis()-now) < 5000){ // 5 seconds
      int looptimer = millis(); 
      IMU = Adafruit10DOF_readData();

      if(IMU.gyroX>gyroMaxErr)
        gyroMaxErr = IMU.gyroX; 
      if(IMU.gyroY>gyroMaxErr)
        gyroMaxErr = IMU.gyroY;
      if(IMU.gyroZ>gyroMaxErr)
        gyroMaxErr = IMU.gyroZ;
      
      while ((millis()-looptimer) < 10); // Loop running at 100 Hz
    }

    Serial.print("Maximum gyro error after calibration: "); Serial.print(gyroMaxErr,4); 
    Serial.print("\r\n");
    Serial.print("Proposed beta: "); Serial.print(0.86602540378*gyroMaxErr,4); // beta = sqrt(3/4) * maximum gyro error
    Serial.print("\r\n");
  #endif
}


void initAdafruit10DOF(uint8_t calibAccMag){
  initI2C();
  delay(100);

  if(calibAccMag){
    Serial.println("Manual calibration selected. Please release button");
    delay(1000); 
    while(digitalRead(BUTTON_PIN));
  }

  Adafruit10DOF_SetupGyro();
  Adafruit10DOF_SetupAcc();
  Adafruit10DOF_SetupMag();
   
  delay(500); // Wait for sensor to stabilise

  calibrateGyro(400); // Calibrate gyroscope

  if(calibAccMag){
    delay(500); // Wait a bit so the printing of text stands out
    // Accelerometer calibration
    Serial.println("Hold button for accelerometer calibration"); 
    delay(3000);
    if(!digitalRead(BUTTON_PIN))
      calibrateAcc(); 

    Serial.println("Hold button for magnetometer calibration"); 
    delay(3000);
    if(!digitalRead(BUTTON_PIN))
      calibrateMag(15);
  }
    

  
}
