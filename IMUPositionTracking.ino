/* Written by Mads Bornebusch 2015.
 This software may be distributed and modified under the terms of the GNU
 General Public License version 2 (GPL2) as published by the Free Software
 Foundation and appearing in the file GPL2.TXT included in the packaging of
 this file. Please note that GPL2 Section 2[b] requires that all works based
 on this software must also be made publicly available under the terms of
 the GPL2 ("Copyleft").
 */

#include <Arduino.h>
#include <Wire.h>
#include "Adafruit10dof.h"
#include "MadgwickAHRS.h"
#include <math.h>
#include "IMUPositionTracking.h"
#include <stdio.h>
#include "SDdataLogging.h"
#include <SD.h>
#include <SPI.h>


// Options for running the beta tests
#define BETATEST 0 //0: No test to find beta, 1: Manual dynamic test, >1: Manual static test

#define BT_DATA_TIME 500 // Time in ms between sending data on Bluetooth

#define MEAN_WINDOW 20 // Window used for running mean on gyro data
#define MEAN_MEAS_WEIGHT (float)(1.0/((float)MEAN_WINDOW)) // Weight on each measurement in the running mean

#define BETA 0.05f // Beta value for the Madgwick filter
#define BETA_HIGH 1.20f // Beta value when the filter is stationary

static const quaternion g0 = {.q0 = 0.0f, .q1 = 0.0f, .q2 = 0.0f, .q3 = 9.82f }; // Gravity


struct IMUdatatype IMU;
uint32_t looptimer;
uint32_t printTimer; 

quaternion accEarthCompensated; 
quaternion startAngle; 

int logflag = 0;
int buttonDisable = 0; 

float Ax, Ay, Az; 
float Vx, Vy, Vz;
float Sx, Sy, Sz;

float absGyroMean = 0.0;
float absGyroMeanOld[MEAN_WINDOW] = {0.0};
int absGyroMeanIndex = 0; 

// TODO: remove or use magnitude instead of coordinates
float AxMean, AyMean, AzMean;
float AxMeanOld[5] = {0.0};
float AyMeanOld[5] = {0.0};
float AzMeanOld[5] = {0.0};
int AmeanIndex;


#if BETATEST
	int state;
	int j=0;
	uint32_t errorTimer;
	float err;
#endif


quaternion quatConj(quaternion q){
	//Source: Page 3 of http://www.emis.de/proceedings/Varna/vol1/GEOM09.pdf
	q.q1 = -q.q1;
	q.q2 = -q.q2;
	q.q3 = -q.q3;
	return q;
}

quaternion quatProd(quaternion a, quaternion b){
	// Source: http://www.x-io.co.uk/res/doc/quaternions.pdf
	// Common sense and rules for ijk-multiplication from http://www.emis.de/proceedings/Varna/vol1/GEOM09.pdf
	// This is a x b. This product is NOT commutative.
	// Multiplication of quaternions is associative: http://en.wikipedia.org/wiki/Associative_property
	quaternion q;
	q.q0 = a.q0*b.q0 - a.q1*b.q1 - a.q2*b.q2 - a.q3*b.q3; 
	q.q1 = a.q0*b.q1 + a.q1*b.q0 + a.q2*b.q3 - a.q3*b.q2;
	q.q2 = a.q0*b.q2 - a.q1*b.q3 + a.q2*b.q0 + a.q3*b.q1;
	q.q3 = a.q0*b.q3 + a.q1*b.q2 - a.q2*b.q1 + a.q3*b.q0; 
	return q;
}

quaternion quatSub(quaternion a, quaternion b){
	// Performs a-b
	quaternion q;
	q.q0 = a.q0-b.q0;
	q.q1 = a.q1-b.q1;
	q.q2 = a.q2-b.q2;
	q.q3 = a.q3-b.q3;

	return q;
}

void integrateAcc(quaternion q, quaternion accData, quaternion gyroData){
	// Source: Rotation Operator Geometry on pg. 5: http://www.emis.de/proceedings/Varna/vol1/GEOM09.pdf
	// Rotating accData so it is in Earth frame coordinates and then subtracts gravity. 
	quaternion accEarth = quatSub(quatProd(quatProd(q,accData),quatConj(q)),g0);
	// This is really in the initial condition frame
	accEarthCompensated = quatProd(quatProd(quatConj(startAngle),accEarth),startAngle);

	Ax = accEarthCompensated.q1; 
	Ay = accEarthCompensated.q2; 
	Az = accEarthCompensated.q3; 

	// Finding magnitude of gyro measurement 
	float absGyro = sqrt(gyroData.q1*gyroData.q1 + gyroData.q2*gyroData.q2 + gyroData.q3*gyroData.q3);

	// New measurement weighted with the measurement weight for running mean
	float absGyroMeanNewMeas = absGyro*MEAN_MEAS_WEIGHT; 

	// Add new gyro mean value to mean and subtract old value
	absGyroMean += (absGyroMeanNewMeas - absGyroMeanOld[absGyroMeanIndex]); 
	
	// Put the new gyro mean value in the array with old measurements
	absGyroMeanOld[absGyroMeanIndex] = absGyroMeanNewMeas; 

	// Increase index variable and make sure it never gets larger than MEAN_WINDOW. 
	if(++absGyroMeanIndex>(MEAN_WINDOW-1))
		absGyroMeanIndex = 0;

	if(absGyroMean>0.5){
		// Movement
		if(getBeta()!=BETA)
			setBeta(BETA);
		Vx += Ax * DT;
		Vy += Ay * DT; 
		Vz += Az * DT; 

	}else{
		// Stationary
		if(getBeta()!=BETA_HIGH)
			setBeta(BETA_HIGH);
		// TODO: Error correction of the velocity and position 
		Vx = 0.0;
		Vy = 0.0; 
		Vz = 0.0;
	}

	Sx += Vx * DT;
	Sy += Vy * DT; 
	Sz += Vz * DT; 

}

void quatPrint(quaternion q){
	Serial.print(q.q0); Serial.print("\t");
	Serial.print(q.q1); Serial.print("\t");
	Serial.print(q.q2); Serial.print("\t");
	Serial.print(q.q3); Serial.print("\t");
	Serial.print("\r\n");
}

void quatPrintFormatted(quaternion q){
	// Printing in the format appropriate for sending the data to the android app
	Serial1.print(q.q0,10); Serial1.print(",");
	Serial1.print(q.q1,10); Serial1.print(",");
	Serial1.print(q.q2,10); Serial1.print(",");
	Serial1.print(q.q3,10); Serial1.print(",");
}

void quat2EulerPrint(quaternion q){
	// Source for the calculation: http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	
	float roll = atan2(2*(q.q0*q.q1+q.q2*q.q3),1-2*(q.q1*q.q1+q.q2*q.q2));
	float pitch = asin(2*(q.q0*q.q2-q.q3*q.q1));
	float yaw = atan2(2*(q.q0*q.q3+q.q1*q.q2),1-2*(q.q2*q.q2+q.q3*q.q3)); 
	
	Serial.print(roll*RAD2DEG); Serial.print("\t");
	Serial.print(pitch*RAD2DEG); Serial.print("\t");
	Serial.print(yaw*RAD2DEG); Serial.print("\t");
	Serial.print("\r\n");
}

float quat2EulerAsError(){
	quaternion q=getQuaternion();
	// Source for the calculation: http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	float roll = atan2(2*(q.q0*q.q1+q.q2*q.q3),1-2*(q.q1*q.q1+q.q2*q.q2));
	// float pitch = asin(2*(q.q0*q.q2-q.q3*q.q1));
	// float yaw = atan2(2*(q.q0*q.q3+q.q1*q.q2),1-2*(q.q2*q.q2+q.q3*q.q3));
	
	return abs(roll*RAD2DEG);
}

#if BETATEST
void findStatBeta(){
	switch (state){
		case 0:
			// Waiting for 2 seconds so the angle have time to stabilise
			if((millis()-errorTimer)>2000){
				Serial.print("Measuring: \t");
				err =0;
				errorTimer = millis();
				state++;
			}
			break;

		case 1:
			err += quat2EulerAsError();
			if((millis()-errorTimer)>2000){
				Serial.print(getBeta(),3); Serial.print("\t"); 
				Serial.print(err); Serial.print("\r\n");

				setBeta(0.01f*(float)(j+1));
				j++;
				errorTimer = millis();
				state =0;

			}
			break;
	}

}
#endif

#if BETATEST
void findDynBeta(){
	quaternion q; 
	switch (state) {
		case 0:
			// Waiting for below 2 degrees
			q=getQuaternion();
		 	if((atan2(2*(q.q0*q.q1+q.q2*q.q3),1-2*(q.q1*q.q1+q.q2*q.q2))*RAD2DEG)<=2.0f){
				Serial.print("<2 deg"); Serial.print("\t");
				state++;
			}
			break;

		case 1:
			// Waiting for above 80 degrees
			q=getQuaternion();
			if((atan2(2*(q.q0*q.q1+q.q2*q.q3),1-2*(q.q1*q.q1+q.q2*q.q2))*RAD2DEG)>=80.0f){
				Serial.print(">80 deg"); Serial.print("\t");
				state++;
			}
			break;

		case 2:
			//Waiting for pushbutton
			if(!digitalRead(BUTTON_PIN)){
				err =0;
				errorTimer = millis();
				state++;
			}
			break;

		case 3:
			// Waiting for measurement to finish
			err += quat2EulerAsError();

			if((millis()-errorTimer)>500){
				Serial.print("Meas. taken"); Serial.print("\t");
				Serial.print(getBeta(),3); Serial.print("\t"); 
				Serial.print(err); Serial.print("\r\n");
				setBeta(0.1f*(float)(j+1));
				//setBeta(0.025f*(float)j+0.5f);

				j++;
				state =0;
			}
		 
			break;
	}	
}
#endif

void send2Bluetooth(){
	Serial1.print("$$$"); // Data header
	Serial1.print("Ang.,"); quatPrintFormatted(getQuaternion());

	Serial1.print("Acc.,"); 
	Serial1.print(Ax); Serial1.print(",");
	Serial1.print(Ay); Serial1.print(",");
	Serial1.print(Az); Serial1.print(",");
	
	Serial1.print("Vel.,"); 
	Serial1.print(Vx); Serial1.print(",");
	Serial1.print(Vy); Serial1.print(",");
	Serial1.print(Vz); Serial1.print(",");
	
	Serial1.print("Pos.,"); 
	Serial1.print(Sx); Serial1.print(",");
	Serial1.print(Sy); Serial1.print(",");
	Serial1.print(Sz); Serial1.print(",");
	Serial1.print("###"); // Data end
	Serial1.print("\r\n");
}


void initAHRS(){
	// Let the AHRS run for two seconds to stabilise
	// We have a high beta here for the acceleration to dominate so the filter stabilises in whatever angle it is in
	looptimer = micros();
	while((micros()-looptimer)<2000000){
		IMU = Adafruit10DOF_readData();
		MadgwickAHRSupdate((float)IMU.gyroX, (float)IMU.gyroY, (float)IMU.gyroZ, (float)IMU.accX, (float)IMU.accY, (float)IMU.accZ, IMU.magX, IMU.magY, IMU.magZ);
		while ((micros()-looptimer) < LOOPTIME);
	}
	setBeta(BETA); // Set beta. 
}


void setup() {
	Serial.begin(115200);
	Serial1.begin(115200);
	pinMode(BUTTON_PIN, INPUT);
	delay(500); // Wait 500ms for everything to be ready


  	initAdafruit10DOF((!digitalRead(BUTTON_PIN))); 
	initAHRS();
	startAngle = getQuaternion();
	quatPrint(startAngle);

	initSD(); 

	Serial.print("Setup OK\r\n");
	Serial1.print("Setup OK\r\n");
}

void loop() {
	looptimer = micros();

	IMU = Adafruit10DOF_readData();
	MadgwickAHRSupdate((float)IMU.gyroX, (float)IMU.gyroY, (float)IMU.gyroZ, (float)IMU.accX, (float)IMU.accY, (float)IMU.accZ, IMU.magX, IMU.magY, IMU.magZ);

	
	#if BETATEST
		#if (BETATEST==1)
			findDynBeta();
		#else
			findStatBeta();
		#endif
	#else

		// Button is released
		if(digitalRead(BUTTON_PIN) && buttonDisable){
			buttonDisable = 0; 
		}
		// Button is pressed while not logging data
		if(!digitalRead(BUTTON_PIN) && !logflag && !buttonDisable){
			buttonDisable = 1; 
			logflag = 1; 
			SDstartLog(); 
		}		
		// Button is pressed while logging data
		if(!digitalRead(BUTTON_PIN) && logflag && !buttonDisable){
			logflag = 0; 
			buttonDisable = 1; 
		}
		
		quaternion accData = {.q0 = 0.0f, .q1 = IMU.accX, .q2 = IMU.accY, .q3 = IMU.accZ};
		quaternion gyroData = {.q0 = 0.0f, .q1 = IMU.gyroX, .q2 = IMU.gyroY, .q3 = IMU.gyroZ};

		integrateAcc(getQuaternion(), accData, gyroData);


		// Logging data
		if(logflag){
			//Serial.println("Logging data"); 
			SDlogData(IMU, getQuaternion()); 
		}
	

		if((millis()-printTimer)>BT_DATA_TIME){
			printTimer = millis();
			
			// Printing data to the Bluetooth serial
			send2Bluetooth(); 

			// For testing:
			quat2EulerPrint(getQuaternion()); 
			/*
			Serial.print("-------------------");
			Serial.print("\r\n");
			quat2EulerPrint(getQuaternion());
			quatPrint(accData);
			quatPrint(accEarthCompensated);
			Serial.print("-------------------");
			Serial.print("\r\n");
			*/

			//Serial.print(micros()-looptimer); Serial.print("\r\n");

		}	
	#endif

	//Serial.print("Time: "); Serial.println((micros()-looptimer));
	while ((micros()-looptimer) < LOOPTIME); // Making sure the loop runs with a fixed frequency
	//Serial.print("Time after: "); Serial.println((micros()-looptimer));
}


