/* Written by Mads Bornebusch 2015.
 This software may be distributed and modified under the terms of the GNU
 General Public License version 2 (GPL2) as published by the Free Software
 Foundation and appearing in the file GPL2.TXT included in the packaging of
 this file. Please note that GPL2 Section 2[b] requires that all works based
 on this software must also be made publicly available under the terms of
 the GPL2 ("Copyleft").
 */

#include <Arduino.h>
#include <SD.h>
#include <SPI.h>
#include "Adafruit10dof.h"
#include "MadgwickAHRS.h"



#define CHIPSELECT 10 // Chipselect pin used

int filenumber = 1;
char filename[15];

void initSD(){
	// Initialising SD card
  	pinMode(10, OUTPUT); // On Arduinos default chip select pin should be output
  	// see if the card is present and can be initialized:
  	if (!SD.begin(CHIPSELECT)) {
    	Serial.println("Card failed, or not present");
    	// Do something to avoid errors later when using the card
  	} else{
  		Serial.println("SD card OK");
  	}
  	
}


void SDstartLog(){
	// Make the header string
	//String dataString = "Acc X, Acc Y, Acc Z, Gyro X, Gyro Y, Gyro Z, Mag X, Mag Y, Mag Z, q0, q1, q2, q3";

	sprintf(filename,"IMU%03d.csv",filenumber);

	// Open the file
	File dataFile = SD.open(filename, FILE_WRITE);

	// if the file is available, write to it:
	if (dataFile) {
		dataFile.println("Acc X, Acc Y, Acc Z, Gyro X, Gyro Y, Gyro Z, Mag X, Mag Y, Mag Z, q0, q1, q2, q3");
		dataFile.close();
	}else {
		Serial.println("Error opening file");
	}

	filenumber++;
}


void SDlogData(IMUdatatype IMU, quaternion q){
	// make a string for assembling the data to log:

	char buffer[200]; 
  	//String dataString = "";

  	sprintf(buffer,"%3.4f,%3.4f,%3.4f,%4.3f,%4.3f,%4.3f,%4.2f,%4.2f,%4.2f,%3.10f,%3.10f,%3.10f,%3.10f",IMU.accX,IMU.accY,IMU.accZ,IMU.gyroX,IMU.gyroY,IMU.gyroZ,IMU.magX,IMU.magY,IMU.magZ,q.q0,q.q1,q.q2,q.q3);

	// Open the file
	File dataFile = SD.open(filename, FILE_WRITE);

	// if the file is available, write to it:
	if (dataFile) {
		dataFile.println(buffer);
		dataFile.close();
	}else {
		Serial.println("Error opening file");
	} 
}
