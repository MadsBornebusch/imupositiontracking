/* Written by Mads Bornebusch 2015.
 This software may be distributed and modified under the terms of the GNU
 General Public License version 2 (GPL2) as published by the Free Software
 Foundation and appearing in the file GPL2.TXT included in the packaging of
 this file. Please note that GPL2 Section 2[b] requires that all works based
 on this software must also be made publicly available under the terms of
 the GPL2 ("Copyleft").
 */


#ifndef _adafruit10dof_h_
#define _adafruit10dof_h_

// Conversion factors used elsewhere
#define DEG2RAD 0.01745329251f // Degress to radians conversion
#define RAD2DEG 57.2957795131f // Radians to degress conversion


struct IMUdatatype{
  float accX, accY, accZ;
  float gyroX, gyroY, gyroZ;
  float magX, magY, magZ;
  float temp;
  float pressure;
  int dt;
};

void initAdafruit10DOF(uint8_t calibAccMag);
struct IMUdatatype Adafruit10DOF_readData();

#endif
