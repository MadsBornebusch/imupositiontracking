/* Written by Mads Bornebusch 2015.
 This software may be distributed and modified under the terms of the GNU
 General Public License version 2 (GPL2) as published by the Free Software
 Foundation and appearing in the file GPL2.TXT included in the packaging of
 this file. Please note that GPL2 Section 2[b] requires that all works based
 on this software must also be made publicly available under the terms of
 the GPL2 ("Copyleft").
 */

#ifndef IMUPositionTracking_h
#define IMUPositionTracking_h

#define BUTTON_PIN 5 // Button pin

// The code will sample the IMU at 200 Hz
#define DT 0.01f // Sample period in seconds 
#define LOOPTIME (int)(DT*1000000) // Sample period in microseconds

#endif